//
//  ildts6wtchApp.swift
//  ildts6wtch Watch App
//
//  Created by Josemaria Carazo Abolafia on 31/3/23.
//

import SwiftUI

@main
struct ildts6wtch_Watch_AppApp: App {
    var body: some Scene {
        WindowGroup {
            AnotherContentView()
        }
    }
}
