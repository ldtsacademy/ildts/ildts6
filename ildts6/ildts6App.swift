//
//  ildts6App.swift
//  ildts6
//
//  Created by Josemaria Carazo Abolafia on 31/3/23.
//

import SwiftUI

@main
struct ildts6App: App {
    var body: some Scene {
        WindowGroup {
            AnotherContentView()
        }
    }
}
