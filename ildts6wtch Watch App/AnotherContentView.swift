//
//  AnotherContentView.swift
//  ildts6
//
//  Created by LDTS ACADEMY on 31/3/23.
//

import SwiftUI

//EN ESTE ARCHIVO HAY 3 OBJETOS -STRUCT- TIPO VIEW -O SEA, 3 VISTAS: View1, View2 y la principal: AnotherContentView
//AnotherContentView es la que se ejecuta (Véase archivo con ildts6App)
//En AnotherContentView hay una variable @State que se define como @Binding en los otros dos Views -View1 y View2. De este modo tengo una variable que es la misma en todas las ventantas... Dicho de otro modo: puedo pasar valores entre ventanas.

struct View2: View {
    @Binding var push: Bool

    var body: some View {
        ZStack {
            Color.black
            Button(action: {
                withAnimation(.easeOut(duration: 0.3)) {
                    self.push.toggle()
                }
            }) {
                Text("POP").foregroundColor(Color.yellow)
            }
        }
        .edgesIgnoringSafeArea(.all)
    }
}

struct View1: View {
    @Binding var push: Bool

    var body: some View {
        ZStack {
            Color.blue
            Button(action: {
                withAnimation(.easeOut(duration: 0.3)) {
                    self.push.toggle()
                }
            }) {
                Text("PUSH")
                    .foregroundColor(Color.white)
            }
        }
        .edgesIgnoringSafeArea(.all)
    }
}

//CONTENT
struct AnotherContentView: View {
    @State private var push = false

    var body: some View {
        ZStack {
            if !push {
                View1(push: $push)
                    .transition(.asymmetric(insertion: .move(edge: .leading), removal: .move(edge: .leading)))
            }

            if push {
                View2(push: $push)
                    .transition(.asymmetric(insertion: .move(edge: .trailing), removal: .move(edge: .trailing)))
            }
        }
    }
}

struct AnotherContentView_Previews: PreviewProvider {
  static var previews: some View {
    AnotherContentView()
  }
}
